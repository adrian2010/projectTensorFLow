from sklearn import datasets
iris = datasets.load_iris()

X = iris.data #caracteristicas
y = iris.target #nombre de Flor

#f(x) = y significa que para cada caracteristica se obtiene una flor especifica
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size = .5)
from sklearn import tree
my_classifier = tree.DecisionTreeClassifier()
my_classifier.fit(X_train,y_train)
predictions = my_classifier.predict(X_test)
print(predictions)