from sklearn.datasets import load_iris
iris = load_iris()
print(iris.feature_names)    #caracteristicas 
print(iris.target_names)     #nombre de las especias 
print(iris.data[0])         #dato de cada caracteristica de la especie 
for i in range(len(iris.target)):
    print("example %d: label %s, featurs %s" % (i,iris.target[i], iris.data[i]))
